add_subdirectory(Missions)
add_subdirectory(MissionsPool)
add_subdirectory(Application)


add_executable(app main.cpp)
target_link_libraries(app PUBLIC application)
