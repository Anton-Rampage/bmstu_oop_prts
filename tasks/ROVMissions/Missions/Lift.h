#pragma once

#include "Dive.h"
#include "Mission.h"

class Lift : public Mission {
 public:
    enum LiftSensorType { DepthSensor = 0, Echo };

    enum LiftMoveType { Spiral = 0, VerticalMove };

 public:
    Lift() = delete;
    Lift(LiftSensorType s_type, uint32_t depth, LiftMoveType d_type);
    ~Lift() override = default;

    bool Check() override;

    std::string Stringify() override;

 private:
    LiftSensorType _s_type;
    uint32_t _depth;
    LiftMoveType _d_type;
};
