#pragma once

#include "Mission.h"

class Return : public Mission {
 public:
    Return();
    ~Return() override = default;

    bool Check() override;

    std::string Stringify() override;
};
