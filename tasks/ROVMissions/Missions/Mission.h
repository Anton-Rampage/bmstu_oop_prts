#pragma once

#include <string>

enum class MissionType { DIVE = 0, LIFT, MOVE, RETURN, UNDEFINED };

class Mission {
 public:
    MissionType TYPE = MissionType::UNDEFINED;

    virtual ~Mission() = default;

    virtual bool Check() = 0;

    virtual std::string Stringify() = 0;
};