#pragma once

#include "Mission.h"

class Move : public Mission {
 public:
    enum MoveType { Point = 0, Line };

    enum DeepMoveType { ConstDepth = 0, ConstDistanceToBottom };

 public:
    Move() = delete;
    Move(uint32_t x, uint32_t y, MoveType m_type, DeepMoveType d_m_type, uint32_t accuracy = 100);
    ~Move() override = default;

    bool Check() override;

    std::string Stringify() override;

 private:
    uint32_t _x;
    uint32_t _y;
    MoveType _m_type;
    DeepMoveType _d_m_type;
    uint32_t _accuracy;
};