#include "Return.h"

bool Return::Check() { return true; }

std::string Return::Stringify() {
    static const std::string result = "Return Mission:\n"
                                      "\tДвижение в стартовую позицию:\n"
                                      "\t\tx = 0\n"
                                      "\t\ty = 0\n";
    return result;
}

Return::Return() {
    Mission::TYPE = MissionType::RETURN;
}
