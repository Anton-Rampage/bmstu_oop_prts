#include "Move.h"

#include <sstream>

Move::Move(uint32_t x, uint32_t y, Move::MoveType m_type, Move::DeepMoveType d_m_type, uint32_t accuracy)
    : _x(x), _y(y), _m_type(m_type), _d_m_type(d_m_type), _accuracy(accuracy) {
    Mission::TYPE = MissionType::MOVE;
}

bool Move::Check() {
    static constexpr uint32_t MaxX = 10000;
    static constexpr uint32_t MinX = 0;
    static constexpr uint32_t MaxY = 10000;
    static constexpr uint32_t MinY = 0;
    return (MinX <= _x && _x <= MaxX) && (MinY <= _y && _y <= MaxY);
}

std::string Move::Stringify() {
    std::stringstream result;
    result << "Move Mission:\n";
    switch (_m_type) {
        case Point:
            result << "\tВыход в точку\n";
            break;
        case Line:
            result << "\tДвижение вдоль прямой\n";
            break;
    }
    switch (_d_m_type) {
        case ConstDepth:
            result << "\tДвижение на постоянной глубине\n";
            break;
        case ConstDistanceToBottom:
            result << "\tДвижение на постоянном отстоянии от дна\n";
            break;
    }

    result << "\tДвижение в точку:\n";
    result << "\t\tx = " << _x << ";\n";
    result << "\t\ty = " << _y << ";\n";
    result << "\tС точностью - " << _accuracy << "\n";

    return result.str();
}
