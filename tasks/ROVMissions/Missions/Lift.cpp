#include "Lift.h"

#include <sstream>

Lift::Lift(LiftSensorType s_type, uint32_t depth, LiftMoveType d_type)
    : _s_type(s_type), _depth(depth), _d_type(d_type) {
    Mission::TYPE = MissionType::LIFT;
}

bool Lift::Check() {
    static constexpr uint32_t MaxDepth = 100;
    static constexpr uint32_t MinDepth = 0;
    return MinDepth <= _depth && _depth <= MaxDepth;
}

std::string Lift::Stringify() {
    std::stringstream result;
    result << "Lift Mission:\n";
    switch (_s_type) {
        case DepthSensor:
            result << "\tЗадание глубины по датчику глубины\n";
            result << "\tГлубина погружения: " << _depth << "\n";
            break;
        case Echo:
            result << "\tЗадание глубины по эхолоту\n";
            result << "\tОтстояние: " << _depth << "\n";
            break;
    }

    switch (_d_type) {
        case Spiral:
            result << "\tПогружение по спирали\n";
            break;
        case VerticalMove:
            result << "\tПогружение за счёт вертикальных движителей\n";
            break;
    }
    return result.str();
}
