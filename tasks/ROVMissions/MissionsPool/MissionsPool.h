#pragma once

#include "Mission.h"

#include <fstream>
#include <memory>
#include <vector>

class MissionsPool {
 public:
    MissionsPool()  = default;
    ~MissionsPool() = default;

    std::string CreateNewFile(const std::string& path);

    bool InsertNewMission(std::unique_ptr<Mission> new_mission);
    bool RemoveMission(uint32_t id);

    bool RepairRemovedMission(uint32_t removed_id, uint32_t insert_id);

    bool SwapMissions(uint32_t l_id, uint32_t r_id);

    bool ChangeMission(uint32_t id, std::unique_ptr<Mission> new_mission);

    bool ShowMission(uint32_t id);
    void ShowAllMissions();
    void ShowDeletedMissions();

    bool IsLastMissionIsReturn();

 private:
    void DropInFileAllMissions();
    void DropInFileLastMission();

 private:
    std::vector<std::unique_ptr<Mission>> _pool;
    std::vector<std::unique_ptr<Mission>> _delete_pool;
    std::string _filename;
    std::fstream _file;
};
