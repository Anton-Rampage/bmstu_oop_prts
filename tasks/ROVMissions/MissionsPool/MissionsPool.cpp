#include "MissionsPool.h"

#include "Mission.h"

#include <iostream>
#include <random>

std::string MissionsPool::CreateNewFile(const std::string& path) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint32_t> dist(1, UINT32_MAX);
    uint32_t missions_id = dist(mt);

    _filename = path + "/Missions" + std::to_string(missions_id) + ".txt";
    _file.open(_filename, std::ios::out);
    _file.close();
    return "Missions" + std::to_string(missions_id) + ".txt";
}

bool MissionsPool::InsertNewMission(std::unique_ptr<Mission> new_mission) {
    _pool.emplace_back(std::move(new_mission));
    DropInFileLastMission();
    return true;
}

bool MissionsPool::RemoveMission(uint32_t id) {
    if (id >= _pool.size()) {
        return false;
    }
    _delete_pool.emplace_back(std::move(_pool[id]));
    _pool.erase(_pool.begin() + id);
    DropInFileAllMissions();
    return true;
}

bool MissionsPool::SwapMissions(uint32_t l_id, uint32_t r_id) {
    if (l_id >= _pool.size() || r_id >= _pool.size()) {
        return false;
    }
    std::iter_swap(_pool.begin() + l_id, _pool.begin() + r_id);
    DropInFileAllMissions();
    return true;
}

bool MissionsPool::ChangeMission(uint32_t id, std::unique_ptr<Mission> new_mission) {
    if (id >= _pool.size()) {
        return false;
    }
    _pool[id] = std::move(new_mission);
    return true;
}

bool MissionsPool::ShowMission(uint32_t id) {
    if (id >= _pool.size()) {
        return false;
    }
    std::cout << _pool[id]->Stringify() << std::endl;
    return true;
}

void MissionsPool::ShowAllMissions() {
    for (size_t id = 0; id < _pool.size(); ++id) {
        std::cout << "Id: " << id << std::endl;
        std::cout << _pool[id]->Stringify() << std::endl;
    }
}

void MissionsPool::DropInFileAllMissions() {
    _file.open(_filename, std::ios::out | std::ios::trunc);
    for (auto& id : _pool) {
        _file << id->Stringify() << std::endl;
    }
    _file.close();
}

void MissionsPool::DropInFileLastMission() {
    _file.open(_filename, std::ios::out | std::ios::app);
    _file << _pool.back()->Stringify() << std::endl;
    _file.close();
}

bool MissionsPool::RepairRemovedMission(uint32_t removed_id, uint32_t insert_id) {
    if (removed_id >= _delete_pool.size() || insert_id > _pool.size() + 1) {
        return false;
    }

    _pool.insert(_pool.begin() + insert_id, std::move(_delete_pool[removed_id]));
    _delete_pool.erase(_delete_pool.begin() + removed_id);
    return true;
}

void MissionsPool::ShowDeletedMissions() {
    for (size_t id = 0; id < _delete_pool.size(); ++id) {
        std::cout << "Id: " << id << std::endl;
        std::cout << _delete_pool[id]->Stringify() << std::endl;
    }
}

bool MissionsPool::IsLastMissionIsReturn() { return _pool.back()->TYPE == MissionType::RETURN; }
