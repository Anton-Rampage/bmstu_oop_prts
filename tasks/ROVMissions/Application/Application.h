#pragma once

#include <iostream>

#include "MissionsPool.h"


enum Actions {
    NEW_MISSION = 0,
    DELETE_MISSION,
    CHANGE_MISSION,
    SWAP_MISSIONS,
    SHOW_MISSIONS,
    REPAIR_MISSION,
    STOP
};

class Application {
 public:
    Application() = default;
    ~Application() = default;
    int Run();

 private:
    void InsertNewMission();
    void DeleteMission();
    void ChangeMission();
    void SwapMissions();
    void RepairMission();
    void ShowMissions();
    bool Stop();

    std::unique_ptr<Mission> CreateNewMission();

    std::unique_ptr<Mission> CreateDiveMission();
    std::unique_ptr<Mission> CreateLiftMission();
    std::unique_ptr<Mission> CreateMoveMission();
    std::unique_ptr<Mission> CreateReturnMission();



 private:
    MissionsPool _pool;
};
