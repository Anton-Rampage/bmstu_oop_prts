#include "Application.h"

#include "Dive.h"
#include "Lift.h"
#include "Move.h"
#include "Return.h"

int Application::Run() {
    std::cout << "Запуск конфигуратора миссий" << std::endl;
    std::cout << "Введите путь к папке в которой должен сохраниться новый файл:" << std::endl;
    std::string file_path;
    std::cin >> file_path;
    std::cout << "Создан новый файл: " << _pool.CreateNewFile(file_path) << std::endl;

    int action_type;

    while (true) {
        std::cout << "\nВыберете одно из возможных действий:\n"
                     "\t0 - Новая миссия\n"
                     "\t1 - Удалить миссию\n"
                     "\t2 - Изменить миссию\n"
                     "\t3 - Изменить порядок миссий\n"
                     "\t4 - Показать миссии\n"
                     "\t5 - Восстановить миссию\n"
                     "\t6 - Закончить работу\n\n";
        std::cin >> action_type;
        switch (action_type) {
            case NEW_MISSION:
                InsertNewMission();
                break;
            case DELETE_MISSION:
                DeleteMission();
                break;
            case CHANGE_MISSION:
                ChangeMission();
                break;
            case SWAP_MISSIONS:
                SwapMissions();
                break;
            case SHOW_MISSIONS:
                ShowMissions();
                break;
            case REPAIR_MISSION:
                RepairMission();
                break;
            case STOP:
                if (Stop()) {
                    return 0;
                }
                break;
            default:
                std::cout << "Неизвестное действие, введите еще раз:\n\n";
                break;
        }
    }
}

std::unique_ptr<Mission> Application::CreateNewMission() {
    std::cout << "Выберите тип миссии которую необходимо добавить:\n"
                 "\t0 - Dive\n"
                 "\t1 - Lift\n"
                 "\t2 - Move\n"
                 "\t3 - Return\n\n";
    int tmp;
    MissionType mission_type;
    while (true) {
        std::cin >> tmp;
        mission_type = static_cast<MissionType>(tmp);
        switch (mission_type) {
            case MissionType::DIVE:
                return CreateDiveMission();
            case MissionType::LIFT:
                return CreateLiftMission();
            case MissionType::MOVE:
                return CreateMoveMission();
            case MissionType::RETURN:
                return CreateReturnMission();
            default:
                std::cout << "Неизвестная миссия, введите еще раз:\n\n";
        }
    }
}

void Application::InsertNewMission() { _pool.InsertNewMission(CreateNewMission()); }

std::unique_ptr<Mission> Application::CreateDiveMission() {
    std::unique_ptr<Mission> dive_mission;
    while (true) {
        int tmp;
        Dive::DiveSensorType dive_sensor_type;
        std::cout << "Введите способ задания глубины:\n"
                     "\t0 - датчик глубины\n"
                     "\t1 - эхолот\n\n";
        while (true) {
            std::cin >> tmp;
            dive_sensor_type = static_cast<Dive::DiveSensorType>(tmp);
            if (dive_sensor_type == Dive::DiveSensorType::DepthSensor ||
                dive_sensor_type == Dive::DiveSensorType::Echo) {
                break;
            }
            std::cout << "Неверный способ задания глубины, введите ещё раз:\n\n";
        }

        int depth = 0;
        std::cout << "Введите глубину погружения(отстояния): " << std::endl;
        std::cin >> depth;

        Dive::DiveMoveType dive_move_type;
        std::cout << "Введите тип погружения\n"
                     "\t0 - по спирали\n"
                     "\t1 - вертикальные движители\n\n";
        while (true) {
            std::cin >> tmp;
            dive_move_type = static_cast<Dive::DiveMoveType>(tmp);
            if (dive_move_type == Dive::DiveMoveType::Spiral || dive_move_type == Dive::DiveMoveType::VerticalMove) {
                break;
            }
            std::cout << "Неверный тип погружения, введите ещё раз:\n\n";
        }
        dive_mission = std::make_unique<Dive>(dive_sensor_type, depth, dive_move_type);
        if (dive_mission->Check()) {
            break;
        }
        std::cout << "Введены не правильные параметры введите еще раз\n\n";
    }
    return dive_mission;
}

std::unique_ptr<Mission> Application::CreateLiftMission() {
    std::unique_ptr<Mission> lift_mission;
    while (true) {
        int tmp;
        Lift::LiftSensorType dive_sensor_type;
        std::cout << "Введите способ задания глубины:\n"
                     "\t0 - датчик глубины\n"
                     "\t1 - эхолот\n\n";
        while (true) {
            std::cin >> tmp;
            dive_sensor_type = static_cast<Lift::LiftSensorType>(tmp);
            if (dive_sensor_type == Lift::LiftSensorType::DepthSensor ||
                dive_sensor_type == Lift::LiftSensorType::Echo) {
                break;
            }
            std::cout << "Неверный способ задания глубины, введите ещё раз:\n\n";
        }

        int depth = 0;
        std::cout << "Введите глубину погружения(отстояния): " << std::endl;
        std::cin >> depth;

        Lift::LiftMoveType dive_move_type;
        std::cout << "Введите тип погружения\n"
                     "\t0 - по спирали\n"
                     "\t1 - вертикальные движители\n\n";
        while (true) {
            std::cin >> tmp;
            dive_move_type = static_cast<Lift::LiftMoveType>(tmp);
            if (dive_move_type == Lift::LiftMoveType::Spiral || dive_move_type == Lift::LiftMoveType::VerticalMove) {
                break;
            }
            std::cout << "Неверный тип погружения, введите ещё раз:\n\n";
        }

        lift_mission = std::make_unique<Lift>(dive_sensor_type, depth, dive_move_type);
        if (lift_mission->Check()) {
            break;
        }
        std::cout << "Введены не правильные параметры введите еще раз\n\n";
    }
    return lift_mission;
}

std::unique_ptr<Mission> Application::CreateMoveMission() {
    std::unique_ptr<Mission> move_mission;
    while (true) {
        int tmp = 0;

        std::cout << "Введите тип выхода\n"
                     "\t0 - выход в точку\n"
                     "\t1 - движение вдоль прямой\n\n"
                  << std::endl;
        Move::MoveType move_type;
        while (true) {
            std::cin >> tmp;
            move_type = static_cast<Move::MoveType>(tmp);
            if (move_type == Move::MoveType::Point || move_type == Move::MoveType::Line) {
                break;
            }
            std::cout << "Неверный тип выхода, введите ещё раз:\n\n";
        }

        std::cout << "Введите тип выхода:\n"
                     "\t0 - на постоянной глубине\n"
                     "\t1 - на постоянном отстоянии от дна\n\n";
        Move::DeepMoveType deep_type;
        while (true) {
            std::cin >> tmp;
            deep_type = static_cast<Move::DeepMoveType>(tmp);
            if (deep_type == Move::DeepMoveType::ConstDepth || deep_type == Move::DeepMoveType::ConstDistanceToBottom) {
                break;
            }
            std::cout << "Неверный тип выхода, введите ещё раз:\n\n";
        }

        int x;
        int y;

        std::cout << "Введите координату X: " << std::endl;
        std::cin >> x;

        std::cout << "Введите координату Y: " << std::endl;
        std::cin >> y;

        move_mission = std::make_unique<Move>(x, y, move_type, deep_type);
        if (move_mission->Check()) {
            break;
        }
        std::cout << "Введены не правильные параметры введите еще раз\n\n";
    }
    return move_mission;
}

std::unique_ptr<Mission> Application::CreateReturnMission() { return std::make_unique<Return>(); }

void Application::ShowMissions() {
    std::cout << "Выберите одно из действий:\n"
                 "\t0 - Показать одну миссию\n"
                 "\t1 - Показать все миссии\n"
                 "\t2 - Показать удаленные миссии\n";
    int act;
    std::cin >> act;
    if (act == 0) {
        std::cout << "Введите id миссии: ";
        int id;
        std::cin >> id;
        _pool.ShowMission(id);
    } else if (act == 1) {
        _pool.ShowAllMissions();
    } else if (act == 2) {
        _pool.ShowDeletedMissions();
    }
}

void Application::DeleteMission() {
    std::cout << "Хотите посмотреть все миссии:\n"
                 "\t0 - нет\n"
                 "\t1 - да\n\n";
    int is_show_mission;
    std::cin >> is_show_mission;
    if (is_show_mission == 1) {
        _pool.ShowAllMissions();
    }

    std::cout << "Введите id миссии которую хотите удалить: ";
    uint32_t id;
    while (true) {
        std::cin >> id;
        if (_pool.RemoveMission(id)) {
            break;
        }
        std::cout << "Введен не правильный id, введите еще раз: ";
    }
}

void Application::ChangeMission() {
    std::cout << "Хотите посмотреть все миссии:\n"
                 "\t0 - нет\n"
                 "\t1 - да\n\n";
    int is_show_mission;
    std::cin >> is_show_mission;
    if (is_show_mission == 1) {
        _pool.ShowAllMissions();
    }

    std::cout << "Введите id миссии которую хотите изменить: ";
    uint32_t id;
    std::cin >> id;
    auto new_mission = CreateNewMission();
    while (!_pool.ChangeMission(id, std::move(new_mission))) {
        std::cout << "Введен не правильный id, введите еще раз: ";
        std::cin >> id;
    }
}

void Application::SwapMissions() {
    std::cout << "Хотите посмотреть все миссии:\n"
                 "\t0 - нет\n"
                 "\t1 - да\n\n";
    int is_show_mission;
    std::cin >> is_show_mission;
    if (is_show_mission == 1) {
        _pool.ShowAllMissions();
    }

    std::cout << "Введите 2 id миссий которую хотите переставить: ";
    uint32_t id_l;
    uint32_t id_r;
    while (true) {
        std::cin >> id_l >> id_r;
        if (_pool.SwapMissions(id_l, id_r)) {
            break;
        }
        std::cout << "Введен не правильный id, введите еще раз: ";
    }
}

void Application::RepairMission() {
    std::cout << "Хотите посмотреть все удаленные миссии:\n"
                 "\t0 - нет\n"
                 "\t1 - да\n\n";
    int is_show_mission;
    std::cin >> is_show_mission;
    if (is_show_mission == 1) {
        _pool.ShowDeletedMissions();
    }

    while (true) {
        std::cout << "Введите id миссий которую хотите переставить восстановить: ";
        uint32_t deleted_id;
        std::cin >> deleted_id;
        std::cout << "Введите id в которое вставить: ";
        uint32_t insert_id;
        std::cin >> insert_id;
        if (_pool.RepairRemovedMission(deleted_id, insert_id)) {
            break;
        }
        std::cout << "Введен не правильный id, введите еще раз\n";
    }
}

bool Application::Stop() {
    if (_pool.IsLastMissionIsReturn()) {
        return true;
    }
    std::cout << "Последней миссией должна быть Return. Хотите ее добавить\n"
                 "\t0 - нет (продолжаем)\n"
                 "\t1 - да (завершение программы)\n";
    int finish;
    std::cin >> finish;
    if (finish == 0) {
        return false;
    }
    _pool.InsertNewMission(std::make_unique<Return>());
    return true;
}