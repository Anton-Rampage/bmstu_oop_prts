#pragma once

#include <iostream>

namespace list {
template <class T>
class Node {
 public:
    T value;
    Node<T> *prev = nullptr;
    Node<T> *next = nullptr;
};

template <class T>
class List {
 public:
    List() = default;
    ~List();

    void push_back(T &&value);
    void pop_back();
    void push_front(T &&value);
    void pop_front();

    void insert(T &&value, size_t index);
    void erase(size_t index);

    template <class Comparator>
    void sort(Comparator comp);

    T &operator[](size_t index);
    [[nodiscard]] size_t size() const { return _size; }

    void print() const;

 private:
    void swap(Node<T> *l, Node<T> *r);

 private:
    Node<T> *_head = nullptr;
    Node<T> *_tail = nullptr;

    size_t _size = 0;
};

template <class T>
List<T>::~List() {
    Node<T> *tmp;
    while (_head) {
        tmp = _head;
        _head = _head->next;
        delete tmp;
    }
}

template <class T>
void List<T>::push_back(T &&value) {
    auto new_node = new Node<T>;
    new_node->value = std::forward<T>(value);
    _size++;
    if (_head == nullptr && _tail == nullptr) {
        _head = new_node;
        _tail = new_node;
        return;
    }
    if (_size == 1) {
        _head->next = new_node;
        _tail = new_node;
        _tail->prev = _head;
        return;
    }
    _tail->next = new_node;
    new_node->prev = _tail;
    _tail = new_node;
}

template <class T>
void List<T>::pop_back() {
    if (_head == nullptr) {
        return;
    }

    if (_size == 1) {
        delete _head;
        _head == nullptr;
        _tail == nullptr;
        _size = 0;
        return;
    }

    auto tmp = _tail->prev;
    delete _tail;
    _tail = tmp;
    _tail->next = nullptr;
    _size--;
}

template <class T>
void List<T>::push_front(T &&value) {
    auto new_node = new Node<T>;
    new_node->value = std::forward<T>(value);
    _size++;
    if (_head == nullptr && _tail == nullptr) {
        _head = new_node;
        _tail = new_node;
        return;
    }
    if (_size == 1) {
        _tail->prev = new_node;
        _head = new_node;
        _head->next = _tail;
        return;
    }
    _head->prev = new_node;
    new_node->next = _head;
    _head = new_node;
}

template <class T>
void List<T>::pop_front() {
    if (_head == nullptr) {
        return;
    }

    if (_size == 1) {
        delete _head;
        _head == nullptr;
        _tail == nullptr;
        _size = 0;
        return;
    }

    auto tmp = _head->next;
    delete _head;
    _head = tmp;
    _head->prev = nullptr;
    _size--;
}

template <class T>
T &List<T>::operator[](size_t index) {
    if (index >= _size) {
        throw std::out_of_range("out of index");
    }
    Node<T> *tmp = _head;
    for (size_t i = 1; i <= index; ++i) {
        tmp = tmp->next;
    }
    return tmp->value;
}

template <class T>
void List<T>::insert(T &&value, size_t index) {
    if (index > _size) {
        throw std::out_of_range("out of index");
    }
    auto new_node = new Node<T>;
    new_node->value = std::forward<T>(value);
    if (_size == 0) {
        _head = new_node;
        _tail = new_node;
        _size++;
        return;
    }
    if (index == _size) {
        new_node->prev = _tail;
        _tail->next = new_node;
        _tail = new_node;
        _size++;
        return;
    }
    if (index == 0) {
        new_node->next = _head;
        _head->prev = new_node;
        _head = new_node;
        _size++;
        return;
    }

    Node<T> *tmp = _head;
    for (size_t i = 1; i <= index; ++i) {
        tmp = tmp->next;
    }
    tmp->prev->next = new_node;
    new_node->next = tmp;
    tmp->prev = new_node;
    _size++;
}

template <class T>
void List<T>::print() const {
    std::cout << "[ ";
    auto tmp = _head;
    std::cout << tmp->value;
    while (tmp->next) {
        std::cout << ", ";
        tmp = tmp->next;
        std::cout << tmp->value;
    }
    std::cout << " ]" << std::endl;
}

template <class T>
void List<T>::erase(size_t index) {
    if (index >= _size) {
        return;
    }
    if (index == _size - 1) {
        this->pop_back();
        return;
    }
    if (index == 0) {
        this->pop_front();
        return;
    }
    Node<T> *tmp = _head;
    for (size_t i = 1; i <= index; ++i) {
        tmp = tmp->next;
    }
    tmp->prev->next = tmp->next;
    tmp->next->prev = tmp->prev;
    delete tmp;
    _size--;
}

template <class T>
void List<T>::swap(Node<T> *l, Node<T> *r) {
    T tmp = l->value;
    l->value = r->value;
    r->value = tmp;
}

template <class T>
template <class Comparator>
void List<T>::sort(Comparator comp) {
    Node<T> *left = _head;
    Node<T> *right = _head->next;

    while (left->next) {
        while (right) {
            if (comp(left->value, right->value)) {
                swap(left, right);
            }
            right = right->next;
        }
        left = left->next;
        right = left->next;
    }
}
}  // namespace list
