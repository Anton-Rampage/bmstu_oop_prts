#include "PniTrax.h"

#include <cstring>
#include <iostream>

namespace pni_trax {

float float_from_big_endian(const unsigned char *raw) {
    uint32_t number = (raw[0] << 24) | (raw[1] << 16) | (raw[2] << 8) | raw[3];
    return *((float *)(&number));
}

std::pair<Status, pni_trax::PniTrax> serialize(const unsigned char *bytes, size_t size) {
    int id = PniTraxId::UNDEFINED;
    pni_trax::PniTrax result;
    size_t i = 0;
    while (i < size) {
        switch (id) {
            case PniTraxId::UNDEFINED:
                id = static_cast<int>(bytes[i]);
                ++i;
                break;
            case PniTraxId::HEADING:
                if (i + sizeof(result.kHeading) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kHeading = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kHeading);
                break;
            case PniTraxId::PITCH:
                if (i + sizeof(result.kPitch) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kPitch = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kPitch);
                break;
            case PniTraxId::ROLL:
                if (i + sizeof(result.kRoll) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kRoll = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kRoll);
                break;
            case PniTraxId::HEADING_STATUS:
                std::memcpy(&result.kHeadingStatus, bytes + i, sizeof(result.kHeadingStatus));
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kHeadingStatus);
                break;
            case PniTraxId::QUATERNION:
                if (i + sizeof(result.kQuaternion) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                for (float &it : result.kQuaternion) {
                    it = float_from_big_endian(bytes + i);
                    i += sizeof(result.kQuaternion[0]);
                }
                id = PniTraxId::UNDEFINED;
                break;
            case PniTraxId::TEMPERATURE:
                if (i + sizeof(result.kTemperature) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kTemperature = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kTemperature);
                break;
            case PniTraxId::DISTORTION:
                std::memcpy(&result.kDistortion, bytes + i, sizeof(result.kDistortion));
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kDistortion);
                break;
            case PniTraxId::CAL_STATUS:
                std::memcpy(&result.kCalStatus, bytes + i, sizeof(result.kCalStatus));
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kCalStatus);
                break;
            case PniTraxId::ACCEL_X:
                if (i + sizeof(result.kAccelX) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kAccelX = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kAccelX);
                break;
            case PniTraxId::ACCEL_Y:
                if (i + sizeof(result.kAccelY) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kAccelY = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kAccelY);
                break;
            case PniTraxId::ACCEL_Z:
                if (i + sizeof(result.kAccelZ) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kAccelZ = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kAccelZ);
                break;
            case PniTraxId::MAG_X:
                if (i + sizeof(result.kMagX) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kMagX = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kMagX);
                break;
            case PniTraxId::MAG_Y:
                if (i + sizeof(result.kMagY) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kMagY = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kMagY);
                break;
            case PniTraxId::MAG_Z:
                if (i + sizeof(result.kMagZ) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kMagZ = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kMagZ);
                break;
            case PniTraxId::GYRO_X:
                if (i + sizeof(result.kGyroX) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kGyroX = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kGyroX);
                break;
            case PniTraxId::GYRO_Y:
                if (i + sizeof(result.kGyroY) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kGyroY = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kGyroY);
                break;
            case PniTraxId::GERO_Z:
                if (i + sizeof(result.kGyroZ) > size) {
                    return {Status::NotCorrectBytesNumbers, result};
                }
                result.kGyroZ = float_from_big_endian(bytes + i);
                id = PniTraxId::UNDEFINED;
                i += sizeof(result.kGyroZ);
                break;
            default:
                return {Status::UndefinedID, result};
        }
    }
    // This is show what field with "id" not fill
    if (id != PniTraxId::UNDEFINED) {
        return {Status::NotCorrectBytesNumbers, result};
    }
    return {Status::OK, result};
}

void PrintPniTrax(const PniTrax &str) {
    std::cout << "kHeading : " << str.kHeading << std::endl;
    std::cout << "kPitch : " << str.kPitch << std::endl;
    std::cout << "kRoll : " << str.kRoll << std::endl;
    std::cout << "kHeadingStatus : " << str.kHeadingStatus << std::endl;
    std::cout << "kQuaternion : [ ";
    for (size_t i = 0; i < sizeof(float); ++i) {
        std::cout << str.kQuaternion[i];
        if (i < sizeof(float) - 1) {
            std::cout << ", ";
        } else {
            std::cout << " ]" << std::endl;
        }
    }
    std::cout << "kTemperature : " << str.kTemperature << std::endl;
    std::cout << "kDistortion : " << str.kDistortion << std::endl;
    std::cout << "kCalStatus : " << str.kCalStatus << std::endl;
    std::cout << "kAccelX : " << str.kAccelX << std::endl;
    std::cout << "kAccelY : " << str.kAccelY << std::endl;
    std::cout << "kAccelZ : " << str.kAccelZ << std::endl;
    std::cout << "kMagX : " << str.kMagX << std::endl;
    std::cout << "kMagY : " << str.kMagY << std::endl;
    std::cout << "kMagZ : " << str.kMagZ << std::endl;
    std::cout << "kGyroX : " << str.kGyroX << std::endl;
    std::cout << "kGyroY : " << str.kGyroY << std::endl;
    std::cout << "kGyroZ : " << str.kGyroZ << std::endl;
}

}  // namespace pni_trax