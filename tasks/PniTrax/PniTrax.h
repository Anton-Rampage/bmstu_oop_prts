#pragma once

#include <string>
#include <vector>

namespace pni_trax {

enum PniTraxId : int {
    HEADING = 5,
    PITCH = 24,
    ROLL = 25,
    HEADING_STATUS = 79,
    QUATERNION = 77,
    TEMPERATURE = 7,
    DISTORTION = 8,
    CAL_STATUS = 9,
    ACCEL_X = 21,
    ACCEL_Y = 22,
    ACCEL_Z = 23,
    MAG_X = 27,
    MAG_Y = 28,
    MAG_Z = 29,
    GYRO_X = 74,
    GYRO_Y = 75,
    GERO_Z = 76,
    UNDEFINED = 0,
};

struct PniTrax {
    float kHeading = 0;
    float kPitch = 0;
    float kRoll = 0;
    char kHeadingStatus = 0;
    float kQuaternion[4] = {};
    float kTemperature = 0;
    bool kDistortion = false;
    bool kCalStatus = false;
    float kAccelX = 0;
    float kAccelY = 0;
    float kAccelZ = 0;
    float kMagX = 0;
    float kMagY = 0;
    float kMagZ = 0;
    float kGyroX = 0;
    float kGyroY = 0;
    float kGyroZ = 0;
};

enum Status { OK, UndefinedID, NotCorrectBytesNumbers };

std::pair<Status, pni_trax::PniTrax> serialize(const unsigned char* bytes, size_t size);

void PrintPniTrax(const PniTrax& str);
}  // namespace pni_trax
