#include "List.h"

#include "gtest/gtest.h"

template <class T>
void assert_list(list::List<T>& result, std::vector<T>& expected) {
    ASSERT_EQ(result.size(), expected.size());
    for (size_t i = 0; i < result.size(); ++i) {
        ASSERT_EQ(result[i], expected[i]);
    }
}

TEST(ListTests, PushBackTest) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.push_back(20);
    test_list.push_back(30);
    std::vector<int> expected = {10, 20, 30};
    assert_list(test_list, expected);
}

TEST(ListTests, PopBackTest) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.push_back(20);
    test_list.push_back(30);
    test_list.pop_back();
    test_list.pop_back();
    test_list.push_back(25);
    std::vector<int> expected = {10, 25};
    assert_list(test_list, expected);
}

TEST(ListTests, GetValue) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.push_back(20);
    test_list.push_back(30);
    ASSERT_EQ(test_list[2], 30);
}

TEST(ListTests, Insert) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.insert(15, 0);
    std::vector<int> expected = {15, 10};
    assert_list(test_list, expected);
}

TEST(ListTests, Erase) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.push_back(20);
    test_list.push_back(30);
    test_list.erase(1);
    std::vector<int> expected = {10, 30};
    assert_list(test_list, expected);
}

TEST(ListTests, Sort) {
    list::List<int> test_list;
    test_list.push_back(20);
    test_list.push_back(10);
    test_list.push_back(30);
    test_list.push_back(11);
    test_list.push_back(60);
    test_list.push_back(45);
    test_list.sort(std::greater<>());
    std::vector<int> expected = {10, 11, 20, 30, 45, 60};
    assert_list(test_list, expected);
}

TEST(ListTests, ThrowTest) {
    list::List<int> test_list;
    test_list.push_back(10);
    test_list.push_back(20);
    test_list.push_back(30);
    ASSERT_THROW(test_list[5], std::out_of_range);
}