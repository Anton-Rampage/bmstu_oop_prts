#include "PniTrax.h"

#include <gtest/gtest.h>

void assert_struct_eq(const pni_trax::PniTrax& first, const pni_trax::PniTrax& second) {
    ASSERT_FLOAT_EQ(first.kHeading, second.kHeading);
    ASSERT_FLOAT_EQ(first.kPitch, second.kPitch);
    ASSERT_FLOAT_EQ(first.kRoll, second.kRoll);
    ASSERT_EQ(first.kHeadingStatus, second.kHeadingStatus);
    for (size_t i = 0; i < sizeof(double); ++i) {
        ASSERT_FLOAT_EQ(first.kQuaternion[i], second.kQuaternion[i]);
    }
    ASSERT_FLOAT_EQ(first.kTemperature, second.kTemperature);
    ASSERT_EQ(first.kDistortion, second.kDistortion);
    ASSERT_EQ(first.kCalStatus, second.kCalStatus);
    ASSERT_FLOAT_EQ(first.kAccelX, second.kAccelX);
    ASSERT_FLOAT_EQ(first.kAccelY, second.kAccelY);
    ASSERT_FLOAT_EQ(first.kAccelZ, second.kAccelZ);
    ASSERT_FLOAT_EQ(first.kMagX, second.kMagX);
    ASSERT_FLOAT_EQ(first.kMagY, second.kMagY);
    ASSERT_FLOAT_EQ(first.kMagZ, second.kMagZ);
    ASSERT_FLOAT_EQ(first.kGyroX, second.kGyroX);
    ASSERT_FLOAT_EQ(first.kGyroY, second.kGyroY);
    ASSERT_FLOAT_EQ(first.kGyroZ, second.kGyroZ);
};

TEST(SerializationTests, __COUNTER__) {
    std::vector<unsigned char> test_heading = {0x05, 0x3F, 0x80, 0x00, 0x00, 0x09, 0x01, 0x1C, 0x40, 0x00, 0x00, 0x00};
    pni_trax::PniTrax expected{.kHeading = 1, .kCalStatus = true, .kMagY = 2};
    auto [status, result] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::OK);
    assert_struct_eq(result, expected);
}

TEST(SerializationTests, __COUNTER__) {
    std::vector<unsigned char> test_heading = {0x4C, 0x41, 0xA4, 0x00, 0x00, 0x4B, 0x3F, 0x91, 0xBC, 0xD8};
    pni_trax::PniTrax expected{.kGyroY = 1.1385756, .kGyroZ = 20.5};
    auto [status, result] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::OK);
    assert_struct_eq(result, expected);
}

TEST(SerializationTests, __COUNTER__) {
    std::vector<unsigned char> test_heading = {0x07, 0x41, 0xA4, 0x00, 0x00, 0x05, 0x3F, 0x91,
                                               0xBC, 0xD8, 0x19, 0x41, 0x35, 0x47, 0xAE};
    pni_trax::PniTrax expected{.kHeading = 1.1385756, .kRoll = 11.33, .kTemperature = 20.5};
    auto [status, result] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::OK);
    assert_struct_eq(result, expected);
}

TEST(SerializationTests, __COUNTER__) {
    std::vector<unsigned char> test_heading = {0x05, 0x43, 0x0D, 0x08, 0x3B, 0x18, 0x3F, 0x91, 0xBC, 0xD8, 0x19,
                                               0x3E, 0xDF, 0x4D, 0xE9, 0x4D, 0x42, 0x28, 0x69, 0xE2, 0x41, 0xF8,
                                               0xF5, 0xC3, 0x41, 0xDE, 0xC2, 0x8F, 0x3C, 0x59, 0x4D, 0x0E};
    pni_trax::PniTrax expected{.kHeading = 141.03215,
                               .kPitch = 1.1385756,
                               .kRoll = 0.43614128,
                               .kQuaternion = {42.1034, 31.12, 27.845, 0.013263}};
    auto [status, result] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::OK);
    assert_struct_eq(result, expected);
}

TEST(SerializationTests, __COUNTER__) {
    std::vector<unsigned char> test_heading = {
        0x05, 0x43, 0x0D, 0x08, 0x3B, 0x18, 0x3F, 0x91, 0xBC, 0xD8, 0x19, 0x3E, 0xDF, 0x4D, 0xE9, 0x4F,
        0x00, 0x08, 0x00, 0x15, 0xBC, 0xA4, 0x08, 0xC0, 0x16, 0x3B, 0xFB, 0x4D, 0xAB, 0x17, 0x3F, 0x80,
        0xF5, 0x08, 0x1B, 0xC1, 0x6E, 0xA4, 0x37, 0x1C, 0xC1, 0x2D, 0x31, 0x27, 0x1D, 0x42, 0x51, 0x3D,
        0x4F, 0x4A, 0x00, 0x00, 0x00, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x00, 0x4C, 0x00, 0x00, 0x00, 0x00};
    pni_trax::PniTrax expected{.kHeading = 141.03215,
                               .kPitch = 1.1385756,
                               .kRoll = 0.43614128,
                               .kHeadingStatus = 0,
                               .kDistortion = false,
                               .kAccelX = -0.0200237,
                               .kAccelY = 0.00766917,
                               .kAccelZ = 1.0074778,
                               .kMagX = -14.915092,
                               .kMagY = -10.8245,
                               .kMagZ = 52.309872,
                               .kGyroX = 0,
                               .kGyroY = 0,
                               .kGyroZ = 0};
    auto [status, result] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::OK);
    assert_struct_eq(result, expected);
}

TEST(SerializationErrorTests, UndefinedID) {
    std::vector<unsigned char> test_heading = {0x02, 0x3F, 0x80, 0x00, 0x00, 0x09, 0x01, 0x1C, 0x40, 0x00, 0x00, 0x00};
    auto [status, _] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::UndefinedID);
}

TEST(SerializationErrorTests, NotCorrectBytesNumbers) {
    std::vector<unsigned char> test_heading = {0x05, 0x3F, 0x80, 0x00, 0x00, 0x09, 0x01, 0x1C, 0x40, 0x00, 0x00};
    auto [status, _] = pni_trax::serialize(test_heading.data(), test_heading.size());
    ASSERT_EQ(status, pni_trax::Status::NotCorrectBytesNumbers);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
